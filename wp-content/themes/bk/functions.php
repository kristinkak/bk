<?php
/**
 * Created by PhpStorm.
 * User: KristinkaK
 * Date: 2017-11-23
 * Time: 14:42
 */
function bk_scripts() {
	wp_enqueue_style( 'styles', get_stylesheet_directory_uri() . '/style.css' );
//	wp_enqueue_script( 'scripts', get_template_directory_uri() . 'js/scripts.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'bk_scripts' );

add_theme_support('post-thumbnails');

function register_my_menus()
{
	register_nav_menus(
		array(
			'header_menu_1' => 'Header Menu',
			'top_menu_1' => 'Top Bar Menu'
		)
	);
}

add_action('init', 'register_my_menus');
function create_post_type()
{
	register_post_type('team',
		array(
			'labels' => array(
				'name' => __('Zespół'),
				'singular_name' => __('Zespół')
			),
			'public' => true,
			'has_archive' => true,
			'supports' => array('title','editor', 'thumbnail'),
		)
	);
	
	register_post_type('bk_calendarium',
		array(
			'labels' => array(
				'name' => __('Kalendarz wydarzeń'),
				'singular_name' => __('Kalendarz')
			),
			'public' => true,
			'has_archive' => true,
			'supports' => array('title', 'editor'),
		)
	);
	register_post_type('offer',
		array(
			'labels' => array(
				'name' => __('Oferta'),
				'singular_name' => __('Oferta')
			),
			'public' => true,
			'has_archive' => true,
			'supports' => array('title'),
		)
	);
}

add_action('init', 'create_post_type');

add_action( 'admin_init', 'hide_editor' );
function hide_editor()
{
	// Get the Post ID.
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];
	if (!isset($post_id)) return;
	// Hide the editor on the page titled 'Homepage'
	$homepgname = get_the_title($post_id);
	if ($homepgname == 'Kancelaria' or $homepgname == 'Oferta' or $homepgname == 'Zespół') {
		remove_post_type_support('page', 'editor');
	}
	// Hide the editor on a page with a specific page template
	// Get the name of the Page Template file.
	$template_file = get_post_meta($post_id, '_wp_page_template', true);
	if ($template_file == 'page-zespol.php') { // the filename of the page template
		remove_post_type_support('page', 'editor');
	}
}