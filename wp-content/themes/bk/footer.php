<footer class="clearfix">
	<div class="row">
		<div class="col-md-3">
			<p class="highlighted">Maria Brzoziewska,Radosław Kędziora</p>
			<p>
				Kancelaria Adwokata i Radcy Prawnego sp. p. </p>
		</div>
		<div class="col-md-3">
			<p class="highlighted">Adres</p>
			<?php the_field('foo_address', 43); ?>
		</div>
		<div class="col-md-3">
			<p class="highlighted">e-mail:</p>
			<?php the_field('foo_email', 43); ?>
			<p class="highlighted">tel/fax:</p>
			<?php the_field('foo_tel', 43); ?>
		</div>
		<div class="col-md-3">
			<p class="highlighted">Godziny otwarcia Kancelarii:</p>
			<?php the_field('foo_open', 43); ?>
			<p class="highlighted">Na życzenie Klientów</p>
			<?php the_field('foo_clients', 43); ?>
		</div>
	</div>
</footer>
<div class="copy col-xs-12">
	<p>Designed by BrzoziewskaKędziora | @ 2017 Wszelkie prawa zastrzeżone.</p>
</div>
<?php wp_footer(); ?>
</div>
</section>
<script>

</script>
</body>
</html>
