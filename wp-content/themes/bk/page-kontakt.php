<?php get_header(); ?>
	<section class="single-page">
	<h1><?php the_title(); ?></h1>
<?php echo do_shortcode('[contact-form-7 id="40" title="Untitled"]');?>
<?php
// Start the loop.
while (have_posts()) : the_post();

	the_content();
endwhile;
?>
</section>
<?php get_footer(); ?>