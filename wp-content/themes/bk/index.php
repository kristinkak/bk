<?php get_header(); ?>
<section class="single-page">
	<h1><?php the_title(); ?></h1>
	<div class="row">
		<div class="col-md-4"><?php the_field('home_first_col', 4); ?></div>
		<div class="col-md-4"><?php the_field('home_second_col', 4); ?></div>
		<div class="col-md-4"><?php the_field('home_third_col', 4); ?></div>
	</div>
	<div class="row" id="calendarium">
		<?php
		$post_array = get_posts(array(
			'posts_per_page' => -1,
			'post_type' => 'bk_calendarium',
			'orderby' => 'date',
			'order' => 'ASC'
		));
		foreach ($post_array as $post) { ?>
			<div class="year col-md-2">
				<span><?php echo $post->post_title; ?></span>
				<div class="event">
					<p><?php echo $post->post_content; ?></p>
				</div>
			</div>
		<?php } ?>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div>
				<p><?php the_field('home_cytat', 4); ?></p>
				<p><?php the_field('home_cytat_autor', 4); ?></p>
			</div>
		</div>
	</div>
	<section id="oferta">
		<h1>Oferta</h1>
		<div class="row">
			<div class="col-md-4"><?php the_field('offer_first', 6); ?></div>
			<div class="col-md-4"><?php the_field('offer_second', 6); ?></div>
			<div class="col-md-4"><?php the_field('offer_third', 6); ?></div>
		</div>
		<div class="row">
			<?php
			$post_array = get_posts(array(
				'posts_per_page' => -1,
				'post_type' => 'offer',
				'orderby' => 'date',
				'order' => 'ASC'
			));
			foreach ($post_array as $offer) { ?>
				<?php
				if ($offer->ID == 60){
					?>
					<div class="col-md-2">
						<div class="box"
							 style="text-align: center; padding:40px;width:200px; height: 200px; display: block; background-image: url(<?php the_field('post_offer_box_background', $offer->ID); ?>);">
							<p style="color:#fff;"><?php echo $offer->post_title; ?></p>
						</div>
					</div>
					<div class="col-md-12" style="display: none">
						<div class="hidden_offer_content">
							<?php the_field('post_offer_content', $offer->ID); ?>
							<div class="specialist_section">
								<img src="<?php the_field('post_offer_specialist_thumb', $offer->ID); ?>"/>
								<?php the_field('post_offer_specialist_name', $offer->ID); ?>
								<?php the_field('post_offer_specialist_role', $offer->ID); ?>
								<p>"<?php the_field('post_offer_specialist_cytat', $offer->ID); ?>"</p>
								<?php the_field('post_offer_specialist_email', $offer->ID); ?>
							</div>
						</div>
					</div>
					<?php
				}
				?>
			<?php } ?>
		</div>
		<div class="row">
			<div class="col-md-4"><?php the_field('offer_fourth', 6); ?></div>
			<div class="col-md-4"><?php the_field('offer_fifth', 6); ?></div>
			<div class="col-md-4"><?php the_field('offer_sixth', 6); ?></div>
		</div>
		<div class="row">
			<?php
			$post_array = get_posts(array(
				'posts_per_page' => -1,
				'post_type' => 'offer',
				'orderby' => 'date',
				'order' => 'ASC'
			));
			foreach ($post_array as $offer) { ?>
				<?php
				if ($offer->ID != 60){
					?>
					<div class="col-md-2">
						<div class="box"
							 style="text-align: center; padding:40px;width:200px; height: 200px; display: block; background-image: url(<?php the_field('post_offer_box_background', $offer->ID); ?>);">
							<p style="color:#fff;"><?php echo $offer->post_title; ?></p>
						</div>
					</div>
					<div class="col-md-12" style="display: none">
						<div class="hidden_offer_content">
							<?php the_field('post_offer_content', $offer->ID); ?>
							<div class="specialist_section">
								<img src="<?php the_field('post_offer_specialist_thumb', $offer->ID); ?>"/>
								<?php the_field('post_offer_specialist_name', $offer->ID); ?>
								<?php the_field('post_offer_specialist_role', $offer->ID); ?>
								<p>"<?php the_field('post_offer_specialist_cytat', $offer->ID); ?>"</p>
								<?php the_field('post_offer_specialist_email', $offer->ID); ?>
							</div>
						</div>
					</div>
					<?php
				}
				?>
				<?php } ?>
		</div>
	</section>

	<?php echo do_shortcode('[contact-form-7 id="40" title="Untitled"]'); ?>
</section>
<?php get_footer(); ?>
