<?php get_header() ?>
<section class="single-page">
	<h1><?php the_title(); ?></h1>
	<div class="row">
		<div class="col-md-4"><?php the_field('team_first_col', 8); ?></div>
		<div class="col-md-4"><?php the_field('team_second_col', 8); ?></div>
		<div class="col-md-4"><?php the_field('team_third_col', 8); ?></div>
	</div>
	<div class="row">


	<?php
	$post_array = get_posts(array(
		'posts_per_page' => -1,
		'post_type' => 'team',
		'orderby' => 'date',
		'order' => 'ASC'
	));
	foreach ($post_array as $post) {
		$post_thumbnail_id = get_post_thumbnail_id($post->ID);
		$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id); ?>
		<div class="single-team-person col-md-2">
			<div class="box" style="width: 200px; height: 200px; background-image: url('<?php echo $post_thumbnail_url;?>');">

			</div>
			<?php
			echo $post->post_title;
			?>
			<p><?php the_field('team_role', $post->ID); ?></p>
			<div class="hidden" style="display: none;">
				<?php the_field('team_number', $post->ID);
				the_field('team_mail', $post->ID); ?>
				<p style="font-style: italic"><?php echo $post->post_content; ?></p>
			</div>
		</div>
	<?php } ?>
	</div>
	<?php echo do_shortcode('[contact-form-7 id="40" title="Untitled"]');?>
</section>
<?php get_footer() ?>
