<?php ?>
	<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php if (is_home()) {
				echo bloginfo("name");
				echo " | ";
				echo bloginfo("description");
			} else {
				echo wp_title(" | ", false, right);
				echo bloginfo("name");
			} ?></title>
		<meta name="description" content="<?php if (is_single()) {
			single_post_title('', true);
		} else {
			bloginfo('name');
			echo " - ";
			bloginfo('description');
		}
		?>"/>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
			  integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
			  crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
				integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
				crossorigin="anonymous"></script>
		<?php wp_head(); ?>
	</head>
<body>
<section class="container">
	<div class="row">
<div class="top-bar">
	<?php
	wp_nav_menu(array(
		'theme_location' => 'top_menu_1'
	)); ?>
</div>
<header>
	<div class="logo col-md-6">
		<a href="<?php echo home_url(); ?>">LOGO</a>
	</div>
	<div class="main_nav col-md-6">
		<button type="button" class="navbar-toggle collapsed burger-btn" id="mobileMenu"
				onclick="jQuery('.menu-header-menu-1-container').toggle();"
				data-toggle="dropdown" data-target="#menu-header-menu-1">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<div class="nav-menu  navbar-collapse colapse">
			<?php
			wp_nav_menu(array(
				'theme_location' => 'header_menu_1'
			)); ?>
		</div>
	</div>
	<?php if (is_page('kontakt') or is_page('kancelaria') or is_page('oferta')) { ?>
		<video width="480" controls>
			<source src="<?php the_field('header_video', 95); ?>" type="video/webm">
			Your browser doesn't support HTML5 video tag.
		</video>
	<?php } ?>
	<?php if (is_page('zespol')) { ?>
		<?php the_field('header_team_cytat', 95); ?>
		<img src="<?php the_field('header_team_photo', 95);?>"/>
	<?php } ?>
	<?php if (is_page('blog')) { ?>
		<?php the_field('header_blog_cytat', 95); ?>
		<img src="<?php the_field('header_blog_photo', 95);?>"/>
	<?php } ?>
	<?php if (is_single()) { ?>
		<img src="<?php the_field('header_blog_single_photo', 95);?>"/>
	<?php } ?>
</header>

<?php
