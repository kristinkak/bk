<?php get_header(); ?>
<section class="single-page">
	<h1><?php the_title(); ?></h1>
	<?php
	$post_array = get_posts(array(
		'posts_per_page' => -1,
		'post_type' => 'post',
		'orderby' => 'date',
		'order' => 'ASC'
	));
	foreach ($post_array as $post) {
		$post_thumbnail_id = get_post_thumbnail_id($post->ID);
		$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id); ?>
		<div class="single-team-person col-md-4">
			<div style="width:100px; height: 100px;background-image: url('<?php echo $post_thumbnail_url;?>');"></div>
			<?php
			echo $post->post_title;
			?>
			<p style="font-style: italic">
				<?php
				$content = $post->post_content;
				$sign = 250;
				if (strlen($content) >= 290) {
					echo "" . substr($content,0,$sign) . "...";
				} else {
					echo $content;
				}
				?>
				</p>
			<a href="<?php echo get_permalink($post->ID);?>">więcej</a>
		</div>
	<?php } ?>
</section>
<?php get_footer(); ?>
